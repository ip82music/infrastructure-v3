// k6 run test_storage.js

import {check, sleep} from 'k6';
import {Rate} from 'k6/metrics';
import http from 'k6/http';

const host = 'https://api.musicstream.app';

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3Q1LjMxNjI5NTI1NzkzNTMxZSszMDZAZ21haWwuY29tIiwic3ViIjoiMDAwYTQzZjEtZDhmOS00Mjc0LWEyOWUtNTMzODJjY2UzMjM2IiwiaWF0IjoxNjA3MDM2MTkxfQ.ucgRKc6vkVoNwx2S3ZgaOn7WYl_L8oW2jhNhkoWbJD8';

export let coverErrors = new Rate('coverErrors');
const image = open('./image.png', 'b');

export let options = {
  stages: [
    { duration: '10s', target: 10 },
    { duration: '30s', target: 10 },
    { duration: '10s', target: 0 },
  ],
  thresholds: {
    http_req_duration: ['p(95)<1500', 'p(90)<1200'], // 95 percent of response times must be below 500ms
    coverErrors: ['rate<0.1'],
  },
};

export default function () {
  const result = http.post(`${host}/cover`, image, {
    headers: {
      'content-type': 'image/png',
      'x-access-token': token
    }
  });
  const isSuccessful = check(result, { 'status is 200': res => res.status === 200 });
  coverErrors.add(!isSuccessful);

  sleep(3);
}