import requests
import time

total_requests = 0
succesful_requests = 0
total_time = 0

for n in range(0, 100):
    print(n)
    started_at = time.time()

    r = requests.get('https://api.musicstream.app/lyrics/Sound%20of%20silence', headers={'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3Q1LjMxNjI5NTI1NzkzNTMxZSszMDZAZ21haWwuY29tIiwic3ViIjoiMDAwYTQzZjEtZDhmOS00Mjc0LWEyOWUtNTMzODJjY2UzMjM2IiwiaWF0IjoxNjA3MDM2MTkxfQ.ucgRKc6vkVoNwx2S3ZgaOn7WYl_L8oW2jhNhkoWbJD8'})
    if r.status_code == 200:
        succesful_requests += 1
    print('status', r.status_code)
    total_requests += 1

    total_time += time.time() - started_at

print('success rate:', succesful_requests / total_requests * 100, '%')
print('avg time:', total_time / total_requests, 'sec')
