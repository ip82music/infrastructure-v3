'use strict';

const fetch = require('node-fetch');

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3Q1LjMxNjI5NTI1NzkzNTMxZSszMDZAZ21haWwuY29tIiwic3ViIjoiMDAwYTQzZjEtZDhmOS00Mjc0LWEyOWUtNTMzODJjY2UzMjM2IiwiaWF0IjoxNjA3MDM2MTkxfQ.ucgRKc6vkVoNwx2S3ZgaOn7WYl_L8oW2jhNhkoWbJD8';

const request = async () => {
    const start = new Date().getTime();
    const response = await fetch('https://api.musicstream.app/lyrics/Sound%20%of%20silence', {
        method: 'GET',
        headers: {
            'x-access-token': token
        }
    })

    console.log(response);

    return {
        isSuccess: response.ok,
        time: new Date().getTime() - start,
    }
}

const avg = (arr) => arr.length ? arr.map((el) => el.time).reduce((acc, el) => acc+=el) / arr.length : 'no value';

const benchmark = async (num) => {
    const result = await Promise.all(new Array(num).fill(request()));
    console.log(`Success percentage ${result.filter((el) => el.isSuccess).length/(num/100)}%`);
    console.log(`Average success request time = ${avg(result.filter((el) => el.isSuccess))} ms`);
    console.log(`Average fail request time = ${avg(result.filter((el) => !el.isSuccess))} ms`);
    console.log(`Average request time = ${avg(result)} ms`);
}

benchmark(100);
